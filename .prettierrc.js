module.exports = {
  printWidth: 100,
  singleQuote: true,
  jsxSingleQuote: true,
  bracketSpacing: true,
  jsxBracketSameLine: false,
  semi: false,
}
