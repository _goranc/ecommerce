import 'react-native-gesture-handler'
import React from 'react'
// libs
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
// store
import { store, persistor } from '@app/store'
// components
import { Navigation } from './navigation'

export const App = (): JSX.Element | null => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  )
}
