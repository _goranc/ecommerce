import React, { memo } from 'react'
// libs
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
// enums
import { EScreenNames } from '@app/enums'

const Tab = createBottomTabNavigator()

export const Navigation = memo((): JSX.Element => {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name={EScreenNames.PRODUCTS} component={() => null} />
        <Tab.Screen name={EScreenNames.CART} component={() => null} />
      </Tab.Navigator>
    </NavigationContainer>
  )
})
