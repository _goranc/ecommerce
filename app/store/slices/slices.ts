// libs
import { persistReducer } from 'redux-persist'
import { combineReducers } from '@reduxjs/toolkit'
import AsyncStorage from '@react-native-async-storage/async-storage'
// enums
import { EWhiteList } from '@app/store/white-list'
import { EBlackList } from '@app/store/black-list'

const PERSIST_CONFIG = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: Object.values(EWhiteList),
  blacklist: Object.values(EBlackList),
}

const rootReducer = combineReducers({})

export default persistReducer(PERSIST_CONFIG, rootReducer)
