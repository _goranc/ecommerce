# Ecommerce React-Native App

### How to run the app?
***
  1. Clone the repo with `git clone https://gitlab.com/_goranc/ecommerce.git`
  2. Move to the app dir `cd ecommerce`   
  3. Checkout on the `dev` branch `git checkout dev`
  4. Install dependencies `yarn install`
  5. Move to `ios` directory `cd ios` and install pods dependencies `pod install`
  6. Move back to `root` directory `cd ..` from `ios` dir
  7. Start the metro server with `yarn start --reset-cache`
  8. Run `yarn react-native run-ios` for the ios side or run `yarn react-native run-android` for the android side

